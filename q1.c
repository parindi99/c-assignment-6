#include <stdio.h>

void pattern(int x);
void Row(int y);
int NoOfRows=1;
int r;

void pattern(int x)
{

    if(x>0)
    {
        Row(NoOfRows);
        printf("\n");
        NoOfRows++;
        pattern(x-1);

    }
}


void Row(int y)
{
    if(y>0)
    {
        printf("%d",y);
        Row(y-1);
    }
}

int main()
{

    printf("Enter the No. of Rows : ");
    scanf("%d",&r);
    pattern(r);
    return 0;
}

